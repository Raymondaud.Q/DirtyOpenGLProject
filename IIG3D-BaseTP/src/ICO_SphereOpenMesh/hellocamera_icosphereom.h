#ifndef ICOSPHERECAMERAOpenMesh_H
#define ICOSPHERECAMERAOpenMesh_H

#include "opengl_object.h"
#include "opengldemo.h"
#include <memory>
#include <functional>
#include "mesh.h"
#include <iostream>

//  OPENMESH FAIT CRASHER L'APPLICATION RIEN QU'AVEC LE TUTORIEL

//#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
//typedef OpenMesh::TriMesh_ArrayKernelT<>  MyMesh;

/** Simple drawing demonstration
 */
class IcoSphereCameraOpenMesh : public OpenGLObject {
public:
    void generateIcoSphereCameraOpenMeshAttributes(unsigned nbDiv, float radius);
    unsigned divideEdge(unsigned a, unsigned b, float radius, glm::vec3 &v1, glm::vec3 &v2, Mesh &tmpMesh);
    explicit IcoSphereCameraOpenMesh(int width, int height, int stackC, int shaderNum );
    ~IcoSphereCameraOpenMesh()override;
    void genSphere(int stackC,int secC);
    void paint();
    void resize(int width, int height){
        OpenGLDemo::resize(width, height);
        _camera->setviewport(glm::vec4(0.f, 0.f, _width, _height));
        _projection = glm::perspective(_camera->zoom(), float(_width) / _height, 0.1f, 100.0f);
    }

    void draw() {
        OpenGLDemo::draw();

        glUseProgram(_program);

        _view = _camera->viewmatrix();

        glUniformMatrix4fv( glGetUniformLocation(_program, "model"), 1, GL_FALSE, glm::value_ptr(_model));
        glUniformMatrix4fv( glGetUniformLocation(_program, "view"), 1, GL_FALSE, glm::value_ptr(_view));
        glUniformMatrix4fv( glGetUniformLocation(_program, "projection"), 1, GL_FALSE, glm::value_ptr(_projection));
        glUniform3f( glGetUniformLocation(_program, "cameraPos"), _camera->position().x , _camera->position().y , _camera->position().z);

        glBindVertexArray(mesh.vao);
        glDrawElements(GL_TRIANGLES, mesh.indices.size(), GL_UNSIGNED_INT, nullptr);
        glBindVertexArray(0);
    }


private:
    Mesh mesh;
    //MyMesh omesh;
    std::map<std::pair<unsigned,unsigned>,unsigned> cache;
    const float c1 = 1.f/sqrtf(1 + 1.61803398875f*1.61803398875f);
    const float c2 = 1.61803398875f*c1;

    // Shader program for rendering
    GLuint _program;

    // matrices
    glm::mat4 _model;
    glm::mat4 _view;
    glm::mat4 _projection;

};

#endif // ICOSPHERECAMERAOpenMesh_H
