#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "myopenglwidget.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_action_Version_OpenGL_triggered();

    void on_actionHello_triangle_triggered();

    void on_actionHello_clear_triggered();

    void on_actionHello_cubes_triggered();

    void on_actionHello_camera_triggered();

    void on_actionHello_uvspheres_triggered();

    void on_actionHello_icospheres_triggered();

    void on_actionHello_OpenMeshIcoSphere_triggered();

    void on_actionBlinn_Shader_triggered();

    void on_actionDiffuse_Shader_triggered();

    void on_actionError_Shader_triggered();

    void on_actionError_Shader_2_triggered();

    void on_actionNormal_Shader_triggered();

    void on_actionCamera_As_Light_Shader_triggered();

    void on_actionHello_Object_triggered();

    void on_actionTouches_triggered();

    void on_actionEulerCamera_triggered();

    void on_actionTrackBall_Camera_triggered();

private:
    Ui::MainWindow *ui;
    MyOpenGLWidget *openglWidget;
};

#endif // MAINWINDOW_H
