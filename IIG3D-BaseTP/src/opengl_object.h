#ifndef OBJECT_H
#define OBJECT_H


#include <vector>
#include "opengldemo.h"
#include "hello_camera/camera.h"
#include "shader_manager/shader_manager.h"
#include <memory>
#include <chrono> 

/** Simple class for managing an OpenGL demo
 */
class OpenGLObject : public OpenGLDemo {

	public:
	    explicit OpenGLObject(int width, int height);
	    //virtual ~OpenGLObject();

	    void mouseclick(int button, float xpos, float ypos) override;
	    void mousemove(float xpos, float ypos) override;
	    void keyboardmove(int key, double time) override;
	    bool keyboard(unsigned char k) override;
	    void setCamera(int k);
	    int getCamera();
        const char* vertexshader_source ;
        const char* fragmentshader_source ;


	protected:
	    // Width and heigth of the viewport
	    float radius = 1.0f;
	    float angle = 0.0f;
	     // for mouse management
	    int _button; // 0 --> left. 1 --> right. 2 --> middle. 3 --> other
	    float _mousex{0};
	    float _mousey{0};
        ShaderManager shaders;

	    // Camera
	    using CameraSelector=std::function<Camera*()>;
	    std::vector<CameraSelector> _cameraselector;
	    unsigned int _activecamera;
        std::unique_ptr<Camera> _camera;
};


#endif 
