#include "myopenglwidget.h"

#include <QMessageBox>
#include <QApplication>
#include <QDateTime>

#include <iostream>
#include <stdexcept>

#include "hello_triangles/hellotriangles.h"
#include "hello_camera/hellocamera.h"
#include "UV_Sphere/hellocamera_sphere.h"
#include "ICO_Sphere/hellocamera_icosphere.h"
#include "ICO_SphereOpenMesh/hellocamera_icosphereom.h"
#include "Cube/hellocamera_cube.h"
#include "Objects/hellocamera_object.h"

MyOpenGLWidget::MyOpenGLWidget(QWidget *parent) :QOpenGLWidget(parent)/*, QOpenGLFunctions_4_1_Core()*/, _openglDemo(nullptr), _lastime(0) {
    // add all demo constructors here
    drawFill = false;
    stackCount = 3;
    sectorCount = 6;
    shaderNum = 0;
    _democonstructors.push_back( [](int width, int height)->OpenGLDemo*{
        std::cout << "Hello clear ..." << std::endl; return new OpenGLDemo(width, height);
        } );
    _democonstructors.push_back( [](int width, int height)->OpenGLDemo*{
        std::cout << "Hello triangles ..." << std::endl; return new SimpleTriangle(width, height);
        } );
    _democonstructors.push_back( [](int width, int height)->OpenGLDemo*{
        std::cout << "Hello camera ..." << std::endl; return new SimpleCamera(width, height);
        } );
    _democonstructors.push_back( [this] (int width, int height)->OpenGLDemo*{
        std::cout << "Hello cube camera ..." << std::endl; return new CubeCamera(width, height, shaderNum);
        } );
    _democonstructors.push_back( [this](int width, int height)->OpenGLDemo*{
        std::cout << "Hello UvSphere camera ..." << std::endl; return new SphereCamera(width, height, stackCount , sectorCount,shaderNum);
        } );
    _democonstructors.push_back( [this](int width, int height)->OpenGLDemo*{
        std::cout << "Hello IcoSphere camera ..." << std::endl; return new IcoSphereCamera(width, height, sectorCount-1,shaderNum);
        } );
    _democonstructors.push_back( [this](int width, int height)->OpenGLDemo*{
        std::cout << "Hello OpenMesh IcoSphere camera..." << std::endl; return new IcoSphereCameraOpenMesh(width, height, sectorCount-1,shaderNum);
        } );

    _democonstructors.push_back( [this](int width, int height)->OpenGLDemo*{
        std::string path = "object.obj";
        std::cout << "Hello object camera..." << std::endl; return new CameraObject(width, height, path ,shaderNum, 2000.f);
        } );
}

MyOpenGLWidget::~MyOpenGLWidget() {

}

QSize MyOpenGLWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize MyOpenGLWidget::sizeHint() const
{
    return QSize(512, 512);
}

void MyOpenGLWidget::cleanup() {
    _openglDemo.reset(nullptr);
}

void MyOpenGLWidget::initializeGL() {
    connect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &MyOpenGLWidget::cleanup);

    if (!initializeOpenGLFunctions()) {
        QMessageBox::critical(this, "OpenGL initialization error", "MyOpenGLWidget::initializeGL() : Unable to initialize OpenGL functions");
        exit(1);
    }
    // Initialize OpenGL and all OpenGL dependent stuff below
    _openglDemo.reset(_democonstructors[0](width(), height()));
}

void MyOpenGLWidget::paintGL() {
    std::int64_t starttime = QDateTime::currentMSecsSinceEpoch();
    _openglDemo->draw();
    glFinish();
    update();
    std::int64_t endtime = QDateTime::currentMSecsSinceEpoch();
    _lastime = endtime-starttime;
}

void MyOpenGLWidget::resizeGL(int width, int height) {
    _openglDemo->resize(width, height);
}

void MyOpenGLWidget::mousePressEvent(QMouseEvent *event) {
    // buttons are 0(left), 1(right) to 2(middle)
    int b;
    Qt::MouseButton button=event->button();
    if (button & Qt::LeftButton) {
        if ((event->modifiers() & Qt::ControlModifier))
            b = 2;
        else
            b = 0;
    } else if (button & Qt::RightButton)
        b = 1;
    else if (button & Qt::MiddleButton)
        b = 2;
    else
        b=3;
    _openglDemo->mouseclick(b, event->x(), event->y());
    _lastime = QDateTime::currentMSecsSinceEpoch();
}

void MyOpenGLWidget::mouseMoveEvent(QMouseEvent *event) {
    _openglDemo->mousemove(event->x(), event->y());
    update();
}

void MyOpenGLWidget::keyPressEvent(QKeyEvent *event) {
    camNum = _openglDemo->getCamera();
    switch(event->key()) {
        // Demo keys

        case Qt::Key_0:
        case Qt::Key_1:
        case Qt::Key_2:
        case Qt::Key_3:
        case Qt::Key_4:
        case Qt::Key_5:
        case Qt::Key_6:
        case Qt::Key_7:
        case Qt::Key_8:
        case Qt::Key_9:
            stackCount = 3;
            sectorCount = 6;
            
            activatedemo(event->key()-Qt::Key_0);
            if ( drawFill )
                _openglDemo->toggledrawmode();

            _openglDemo->setCamera(camNum);
            update();
        break;

        // Move keys
        case Qt::Key_Left:
        case Qt::Key_Up:
        case Qt::Key_Right:
        case Qt::Key_Down:
            _openglDemo->keyboardmove(event->key()-Qt::Key_Left, 1./100/*double(_lastime)/10.*/);
            update();
            break;

        case Qt::Key_Plus:
            stackCount ++;
            sectorCount ++;
            activatedemo(numDemo);
            if ( drawFill )
                _openglDemo->toggledrawmode();
            _openglDemo->setCamera(camNum);
            update();
            break;

        case Qt::Key_Minus:
            if ( stackCount > 3 && sectorCount >3 ){
                stackCount --;
                sectorCount --;
                activatedemo(numDemo);
                if ( drawFill )
                    _openglDemo->toggledrawmode();
                _openglDemo->setCamera(camNum);
                update();
            }
            break;

        case Qt::Key_Enter:
            drawFill = ! drawFill;
            _openglDemo->toggledrawmode();
            update();
            break;

        case Qt::Key_Period:
            shaderNum = (shaderNum+1) % 6;
            swapshader(shaderNum);

        break;
        // Wireframe key
        case Qt::Key_W:
            _openglDemo->toggledrawmode();
            update();
        break;
        // Other keys are transmitted to the scene
        default :
            if (_openglDemo->keyboard(event->text().toStdString()[0]))
                update();
        break;
    }
}

void MyOpenGLWidget::activatedemo(unsigned int numdemo) {
    if (numdemo < _democonstructors.size()) {
        numDemo = numdemo;
        std::cout << "Activating demo " << numdemo << " : ";
        makeCurrent();
        _openglDemo.reset(_democonstructors[numdemo](width(), height()));
        doneCurrent();
        update();
    }
}

void MyOpenGLWidget::swapshader(unsigned int numshader) {
    int camNum = _openglDemo->getCamera();
    shaderNum = numshader;
    std::cout << "SHADER " << shaderNum << std::endl;
    activatedemo(numDemo);
    if ( drawFill )
        _openglDemo->toggledrawmode();
    _openglDemo->setCamera(camNum);
}

void MyOpenGLWidget::swapcamera(unsigned int numCamera) {
    camNum = numCamera;
    std::cout << "Camera " << numCamera << std::endl;
    activatedemo(numDemo);
    if ( drawFill )
        _openglDemo->toggledrawmode();
    _openglDemo->setCamera(numCamera);
}

