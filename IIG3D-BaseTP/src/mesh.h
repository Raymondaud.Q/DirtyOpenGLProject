#ifndef IIG3D_BASETP_MESH_H
#define IIG3D_BASETP_MESH_H

#include <vector>
#include "opengldemo.h"

class Mesh {
    public:
        std::vector<GLfloat> vertices;
        std::vector<GLfloat> normals;
        std::vector<GLuint> indices;
        GLuint vao;
        GLuint vbo;
        GLuint nbo;
        GLuint ebo;

        void addTri(unsigned a, unsigned b, unsigned c);
        void addQuad(unsigned a, unsigned b, unsigned c, unsigned d);
        void addVertex(glm::vec3& v);
        void addVertex(float x, float y, float z);
        unsigned nbTriangles();
        void addNormal(float x, float y, float z);
        unsigned nbVertices();
        void clear();
};


#endif //IIG3D_BASETP_MESH_H
