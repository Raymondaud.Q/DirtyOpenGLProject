#ifndef SPHERECAMERA_H
#define SPHERECAMERA_H

#include "opengl_object.h"
#include "opengldemo.h"
#include <memory>
#include <functional>
#include "mesh.h"


/** Simple drawing demonstration
 */
class SphereCamera : public OpenGLObject {
public:
    explicit SphereCamera(int width, int height, int stackC , int secC, int shaderNum);
    ~SphereCamera() override;
    void genSphere(int stackC,int secC);
    void paint();
    void resize(int width, int height){
        OpenGLDemo::resize(width, height);
        _camera->setviewport(glm::vec4(0.f, 0.f, _width, _height));
        _projection = glm::perspective(_camera->zoom(), float(_width) / _height, 0.1f, 100.0f);
    }

    void draw() {
        OpenGLDemo::draw();

        glUseProgram(_program);

        _view = _camera->viewmatrix();

        glUniformMatrix4fv( glGetUniformLocation(_program, "model"), 1, GL_FALSE, glm::value_ptr(_model));
        glUniformMatrix4fv( glGetUniformLocation(_program, "view"), 1, GL_FALSE, glm::value_ptr(_view));
        glUniformMatrix4fv( glGetUniformLocation(_program, "projection"), 1, GL_FALSE, glm::value_ptr(_projection));
        glUniform3f( glGetUniformLocation(_program, "cameraPos"), _camera->position().x , _camera->position().y , _camera->position().z);

        glBindVertexArray(mesh.vao);
        glDrawElements(GL_TRIANGLES, mesh.indices.size(), GL_UNSIGNED_INT, nullptr);
        glBindVertexArray(0);
    }


private:
    Mesh mesh;

    // Shader program for rendering
    GLuint _program;

    // matrices
    glm::mat4 _model;
    glm::mat4 _view;
    glm::mat4 _projection;
};

/*------------------------------------------------------------------------------------------------------------------------*/


#endif // SPHERECAMERA_H
