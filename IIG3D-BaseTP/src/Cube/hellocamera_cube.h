#ifndef CUBECAMERA_H
#define CUBECAMERA_H

#include "opengl_object.h"
#include "opengldemo.h"
#include "hello_camera/camera.h"
#include <memory>
#include <functional>


/** Simple drawing demonstration
 */
class CubeCamera : public OpenGLObject {
public:
    explicit CubeCamera(int width, int height, int shaderNum);
    ~CubeCamera() override;
    
    void resize(int width, int height){
        OpenGLDemo::resize(width, height);
        _camera->setviewport(glm::vec4(0.f, 0.f, _width, _height));
        _projection = glm::perspective(_camera->zoom(), float(_width) / _height, 0.1f, 100.0f);
    }

    void draw() {
        OpenGLDemo::draw();

        glUseProgram(_program);

        _view = _camera->viewmatrix();

        glUniformMatrix4fv( glGetUniformLocation(_program, "model"), 1, GL_FALSE, glm::value_ptr(_model));
        glUniformMatrix4fv( glGetUniformLocation(_program, "view"), 1, GL_FALSE, glm::value_ptr(_view));
        glUniformMatrix4fv( glGetUniformLocation(_program, "projection"), 1, GL_FALSE, glm::value_ptr(_projection));
        glUniform3f( glGetUniformLocation(_program, "cameraPos"), _camera->position().x , _camera->position().y , _camera->position().z);
        /*int loc = glGetUniformLocation(_program, "cameraPos");
        if(loc != -1)
            glUniform3f(loc, _camera->position().x , _camera->position().y , _camera->position().z);
        else
            std::cerr << "Unable to locate 'cameraPos' Uniform in shader : " << _program << std::endl;*/

        glBindVertexArray(_vao);
        glDrawElements(GL_TRIANGLES, _indices.size(), GL_UNSIGNED_INT, nullptr);
        glBindVertexArray(0);
    }


private:
    // A simple geometry
    std::vector<GLfloat> _vertices;
    std::vector<GLfloat> _normals;
    std::vector<GLuint> _indices;

    // OpenGL object for geometry
    GLuint _vao;
    GLuint _vbo;
    GLuint _nbo;
    GLuint _ebo;

    // Shader program for rendering
    GLuint _program;

   

    // matrices
    glm::mat4 _model;
    glm::mat4 _view;
    glm::mat4 _projection;
};

/*------------------------------------------------------------------------------------------------------------------------*/


#endif // SIMPLECAMERA_H
