#include "mainwindow.h"

#include "ui_mainwindow.h"
#include "shader_manager/shader_manager.h"

#include <QMessageBox>
#include <QVBoxLayout>

#include <iostream>
#include <sstream>
#include <iomanip>
#include <iostream>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {

    QSurfaceFormat format;
    format.setVersion(4, 1);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setDepthBufferSize(24);
    QSurfaceFormat::setDefaultFormat(format);

    ui->setupUi(this);

    openglWidget = new MyOpenGLWidget(this);
    openglWidget->resize(openglWidget->sizeHint());
    openglWidget->setFocus();

    setCentralWidget(openglWidget);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_action_Version_OpenGL_triggered() {
    std::stringstream message;
    message << "Qt version     : " << qVersion() << std::endl;
    message << "Renderer       : " << glGetString(GL_RENDERER) << std::endl;
    message << "Vendor         : " << glGetString(GL_VENDOR) << std::endl;
    message << "Version        : " << glGetString(GL_VERSION) << std::endl;
    message << "GLSL Version   : " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    QMessageBox::information(this, "OpenGL Information", message.str().c_str());
}

void MainWindow::on_actionHello_clear_triggered() {
    openglWidget->activatedemo(0);
}

void MainWindow::on_actionHello_triangle_triggered() {
    openglWidget->activatedemo(1);
}

void MainWindow::on_actionHello_camera_triggered() {
    openglWidget->activatedemo(2);
}

void MainWindow::on_actionHello_cubes_triggered() {
    openglWidget->activatedemo(3);
}

void MainWindow::on_actionHello_uvspheres_triggered() {
    openglWidget->activatedemo(4);
}

void MainWindow::on_actionHello_icospheres_triggered() {
    openglWidget->activatedemo(5);
}

void MainWindow::on_actionHello_OpenMeshIcoSphere_triggered()
{
    openglWidget->activatedemo(6);
}


void MainWindow::on_actionBlinn_Shader_triggered()
{
    openglWidget->swapshader(0);
}

void MainWindow::on_actionDiffuse_Shader_triggered()
{
    openglWidget->swapshader(1);
}

void MainWindow::on_actionError_Shader_triggered()
{
    openglWidget->swapshader(2);
}

void MainWindow::on_actionError_Shader_2_triggered()
{
    openglWidget->swapshader(3);
}

void MainWindow::on_actionNormal_Shader_triggered()
{
    openglWidget->swapshader(4);
}

void MainWindow::on_actionCamera_As_Light_Shader_triggered()
{
    openglWidget->swapshader(5);
}

void MainWindow::on_actionHello_Object_triggered()
{
    openglWidget->activatedemo(7);
}

void MainWindow::on_actionTouches_triggered()
{
    std::stringstream message;
    message << " Choisissez votre Mesh avec [ 0 , 1 , 2 , ... ] \n";
    message << " Choisissez votre Shader avec le point ( . ) \n";
    message << " Subdivisez/Reduisez vos sphères avec plus et moins (  + , - ) \n";
    message << " Changez de camera avec la touche p ( p ) \n";
    message << " Appuyez sur la touche entrée pour voir les arrêtes (  Enter ) \n";
    message << " Bougez avec les fléches ou la souris \n";
    QMessageBox::information(this, " Info Keyboard", message.str().c_str());
}

void MainWindow::on_actionEulerCamera_triggered()
{
    openglWidget->swapcamera(0);
}

void MainWindow::on_actionTrackBall_Camera_triggered()
{
    openglWidget->swapcamera(1);
}
