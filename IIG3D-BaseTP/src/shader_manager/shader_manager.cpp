#ifndef SHADERMANAGER_CPP
#define SHADERMANGER_CPP
#include"shader_manager/shader_manager.h"
ShaderManager::ShaderManager()
{
    fragShaders.push_back(blinnShader);
    fragShaders.push_back(diffusShader);
    fragShaders.push_back(eclairageShader);
    fragShaders.push_back(errShader);
    fragShaders.push_back(normShader);
    fragShaders.push_back(camera_as_light_Shader);
}

std::string* ShaderManager::getFragmentShader(int i){
    fragmentShader = fragShaders[i%fragShaders.size()];
    return &fragmentShader;
}

#endif
