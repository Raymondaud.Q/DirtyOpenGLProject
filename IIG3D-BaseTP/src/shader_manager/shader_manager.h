#ifndef SHADERMANAGER_H
#define SHADERMANGER_H

#include <string>
#include <iostream>
#include <vector>

static char* vertShader =
"#version 410 core\n\
layout (location = 0) in vec3 position;\n\
layout (location = 1) in vec3 inormal;\n\
uniform mat4 model;\n\
uniform mat4 view;\n\
uniform mat4 projection;\n\
out vec3 normal;\n\
out vec3 errPos;\n\
void main()\n\
{\n\
    // Note that we read the multiplication from right to left\n\
    gl_Position = projection * view * model * vec4(position, 1.0f);\n\
    normal = inormal;\n\
    errPos = position;\n\
};\n";

static char* errShader =
"#version 410 core\n\
in vec3 normal;\n\
in vec3 errPos;\n\
out vec4 color;\n\
void main()\n\
{\n\
    float coeffErr =( 1 - length(errPos))*50;\n\
    color = normalize(vec4( (0.3/(5-coeffErr))*2, 0.85*(1-coeffErr), 0.75/(1-coeffErr), 1));\n\
};\n";

static char* normShader =
"#version 410 core\n\
in vec3 normal;\n\
in vec3 errPos;\n\
out vec4 color;\n\
void main()\n\
{\n\
    color = vec4(normalize(normal)*0.5+0.5, 1.0);\n\
};\n";

static char* eclairageShader =
"#version 410 core\n\
in vec3 normal;\n\
in vec3 errPos;\n\
out vec4 color;\n\
void main()\n\
{\n\
    color = vec4(vec3(clamp(dot(normalize(normal), vec3(0,0,1)), 0, 1)), 1.0);\n\
};\n";

static char* diffusShader =
"#version 410 core\n\
in vec3 normal;\n\
in vec3 errPos;\n\
out vec4 color;\n\
vec3 lightPos = vec3(3.14, 10.0, 6);\n\
vec3  lightColor = vec3(0.80,1,0.80);\n\
vec3 objectColor= vec3(0.40, 1 ,1);\n\
float ambientStrength = 0.1;\n\
void main()\n\
{\n\
    vec3 norm = normalize(normal);\n\
    vec3 lightDir = normalize(lightPos - errPos);\n\
    float diff = max(dot(norm, lightDir), 0.0);\n\
    vec3 diffuse = diff * lightColor;\n\
    vec3 ambient = ambientStrength * lightColor;\n\
    vec3 result = (ambient + diffuse) * objectColor;\n\
    color = vec4(result, 1.0);\n\
};\n";

static char*  blinnShader =
"#version 410 core\n\
in vec3 normal;\n\
in vec3 errPos;\n\
out vec4 color;\n\
uniform vec3 cameraPos;\n\
vec3 lightPos = vec3(3.14, 10.0, 6);\n\
vec3 specularColor= vec3(1, 1 ,1);\n\
vec3  diffuseColor = vec3(1,1,1);\n\
float shininessFactor = 0.2;\n\
float attenIntensity = 0.9;\n\
float ambientIntensity = 0.5;\n\
void main() {\n\
    vec3 lightDir = lightPos-errPos;\n\
    //blinn term\n\
    float cosAngIncidence = dot(normal, lightDir);\n\
    vec3 viewDirection = normalize(-cameraPos);\n\
    vec3 halfAngle = normalize(lightDir + viewDirection);\n\
    float blinnTerm = dot(normal, halfAngle);\n\
    cosAngIncidence = clamp(cosAngIncidence, 0, 1);\n\
    blinnTerm = clamp(blinnTerm, 0, 1);\n\
    blinnTerm = cosAngIncidence != 0.0 ? blinnTerm : 0.0;\n\
    blinnTerm = pow(blinnTerm, shininessFactor);\n\
    vec3 outColor =\n\
    (diffuseColor * attenIntensity * cosAngIncidence) +\n\
    (specularColor * attenIntensity * blinnTerm) +\n\
    (diffuseColor * ambientIntensity);\n\
    color = vec4(outColor, 1.);\n\
};\n";

static char* camera_as_light_Shader =
"#version 410 core\n\
in vec3 normal;\n\
in vec3 errPos;\n\
out vec4 color;\n\
uniform vec3 cameraPos;\n\
vec3  lightColor = vec3(1,0.80,0.80);\n\
vec3 objectColor= vec3(1, 0.60 ,0.1);\n\
float ambientStrength = 0.1;\n\
void main()\n\
{\n\
    vec3 norm = normalize(normal);\n\
    vec3 lightDir = normalize(cameraPos - errPos);\n\
    float diff = max(dot(norm, lightDir), 0.0);\n\
    vec3 diffuse = diff * lightColor;\n\
    vec3 ambient = ambientStrength * lightColor;\n\
    vec3 result = (ambient + diffuse) * objectColor;\n\
    color = vec4(result, 1.0);\n\
};\n";

class ShaderManager{
    public :

        std::vector<std::string> fragShaders;
        std::string fragmentShader = blinnShader;
        std::string vertexShader = vertShader;

        std::string* getFragmentShader(int i);
        ShaderManager();
};

#endif
