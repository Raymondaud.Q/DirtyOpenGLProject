#include "hellocamera_icosphere.h"
#include <iostream>

IcoSphereCamera::IcoSphereCamera(int width, int height, int stackC, int shaderNum ) : OpenGLObject(width, height) {
    shaders = ShaderManager();
// Record start time
    auto start = std::chrono::high_resolution_clock::now();
    generateIcoSphereCameraAttributes(stackC-5,1.0f);
    vertexshader_source = (shaders.vertexShader).c_str();
    std::string frag = *shaders.getFragmentShader(shaderNum);
    fragmentshader_source = frag.c_str();
    paint();
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Elapsed time : " << elapsed.count() << " s\n";
    std::cout << " Subdivision n°" << stackC-5 << std::endl;
    //_model = glm::scale(_model,glm::vec3(0.5f,0.25f,1.0f));
}

void IcoSphereCamera::paint()
{
    // Initialize shaders
    GLint success;
    GLchar infoLog[512]; // warning fixed size ... request for LOG_LENGTH!!!
    GLuint vertexshader, fragmentshader;
    // Initialize the geometry
    // 1. Generate geometry buffers
    glGenBuffers(1, &mesh.vbo);
    glGenBuffers(1, &mesh.nbo);
    glGenBuffers(1, &mesh.ebo);
    glGenVertexArrays(1, &mesh.vao);
    // 2. Bind Vertex Array Object
    glBindVertexArray(mesh.vao);
    // 3. Copy our vertices array in a buffer for OpenGL to use
    glBindBuffer(GL_ARRAY_BUFFER, mesh.vbo);
    glBufferData(GL_ARRAY_BUFFER, mesh.vertices.size() * sizeof(GLfloat), mesh.vertices.data(), GL_STATIC_DRAW);
    // 4. Then set our vertex attributes pointers
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid *) 0);
    glEnableVertexAttribArray(0);
    // 5. Copy our normals array in a buffer for OpenGL to use
    glBindBuffer(GL_ARRAY_BUFFER, mesh.nbo);
    glBufferData(GL_ARRAY_BUFFER, mesh.normals.size() * sizeof(GLfloat), mesh.normals.data(), GL_STATIC_DRAW);
    // 6. Copy our vertices array in a buffer for OpenGL to use
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid *) 0);
    glEnableVertexAttribArray(1);
    // 7. Copy our index array in a element buffer for OpenGL to use
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.indices.size() * sizeof(GLfloat), mesh.indices.data(), GL_STATIC_DRAW);
    //6. Unbind the VAO
    glBindVertexArray(0);
    // 1. Generate the shader
    vertexshader = glCreateShader(GL_VERTEX_SHADER);
    // 2. set the source
    glShaderSource(vertexshader, 1, &vertexshader_source, NULL);
    // 3. Compile
    glCompileShader(vertexshader);
    // 4. test for compile error
    glGetShaderiv(vertexshader, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(vertexshader, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    }

    fragmentshader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentshader, 1, &fragmentshader_source, NULL);
    glCompileShader(fragmentshader);
    glGetShaderiv(fragmentshader, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(fragmentshader, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }

    // 1. Generate the program
    _program = glCreateProgram();
    // 2. Attach the shaders to the program
    glAttachShader(_program, vertexshader);
    glAttachShader(_program, fragmentshader);
    // 3. Link the program
    glLinkProgram(_program);
    // 4. Test for link errors
    glGetProgramiv(_program, GL_LINK_STATUS, &success);
    if(!success) {
        glGetProgramInfoLog(_program, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::LINK_FAILED\n" << infoLog << std::endl;
    }
    glDeleteShader(vertexshader);
    glDeleteShader(fragmentshader);
    _view = _camera->viewmatrix();
    _projection = glm::perspective(_camera->zoom(), float(_width) / _height, 0.1f, 100.0f);
}


IcoSphereCamera::~IcoSphereCamera() {
    glDeleteProgram(_program);
    glDeleteBuffers(1, &mesh.vbo);
    glDeleteBuffers(1, &mesh.nbo);
    glDeleteBuffers(1, &mesh.ebo);
    glDeleteVertexArrays(1, &mesh.vao) ;
}

unsigned IcoSphereCamera::divideEdge(unsigned a, unsigned b, float radius, glm::vec3 &v1, glm::vec3 &v2, Mesh &tmpMesh) {
    if (a < b) {
        std::swap(a, b);
    }
    auto edge = std::pair<unsigned, unsigned>(a, b);
    auto it = cache.find(edge);
    if (it != cache.end()) {
        return it->second;
    }

    glm::vec3 v = glm::normalize((v1 + v2) / 2.f) * radius;

    unsigned ret = tmpMesh.nbVertices();
    tmpMesh.addVertex(v);
    cache.emplace(edge, ret);

    return ret;
}



void IcoSphereCamera::generateIcoSphereCameraAttributes(unsigned nbDiv, float radius) {
    mesh.clear();

    mesh.vertices = {
            c1 * radius, c2 * radius, 0,
            c2 * radius, 0, c1 * radius,
            0, c1 * radius, c2 * radius,

            -c1 * radius, c2 * radius, 0,
            0, c1 * radius, -c2 * radius,
            c2 * radius, 0, -c1 * radius,

            c1 * radius, -c2 * radius, 0,
            0, -c1 * radius, c2 * radius,
            -c2 * radius, 0, c1 * radius,

            -c2 * radius, 0, -c1 * radius,
            0, -c1 * radius, -c2 * radius,
            -c1 * radius, -c2 * radius, 0
    };

    mesh.normals = mesh.vertices;

    mesh.indices = {
            0, 2, 1,
            0, 1, 5,
            0, 5, 4,
            0, 4, 3,
            0, 3, 2,

            11, 6, 10,
            11, 7, 6,
            11, 8, 7,
            11, 9, 8,
            11, 10, 9,

            1, 7, 6,
            5, 6, 10,
            4, 10, 9,
            3, 9, 8,
            2, 8, 7,

            10, 5, 4,
            9, 4, 3,
            8, 2, 3,
            7, 1, 2,
            6, 5, 1
    };

    for (unsigned div = 0; div < nbDiv; ++div) {
        Mesh tmpMesh;
        tmpMesh.vertices = mesh.vertices;
        cache.clear();
        for (unsigned i = 0; i < mesh.indices.size(); i += 3) {
            unsigned a = mesh.indices[i];
            unsigned b = mesh.indices[i + 1];
            unsigned c = mesh.indices[i + 2];
            glm::vec3 v0 = glm::vec3(mesh.vertices[a * 3], mesh.vertices[a * 3 + 1], mesh.vertices[a * 3 + 2]);
            glm::vec3 v1 = glm::vec3(mesh.vertices[b * 3], mesh.vertices[b * 3 + 1], mesh.vertices[b * 3 + 2]);
            glm::vec3 v2 = glm::vec3(mesh.vertices[c * 3], mesh.vertices[c * 3 + 1], mesh.vertices[c * 3 + 2]);
            unsigned d = divideEdge(a, b, radius, v0, v1, tmpMesh);
            unsigned e = divideEdge(b, c, radius, v1, v2, tmpMesh);
            unsigned f = divideEdge(c, a, radius, v2, v0, tmpMesh);
            tmpMesh.addTri(a, d, f);
            tmpMesh.addTri(d, b, e);
            tmpMesh.addTri(e, c, f);
            tmpMesh.addTri(d, e, f);
        }
        tmpMesh.normals = tmpMesh.vertices;
        mesh = tmpMesh;
    }
    std::cout << "\ttriangles : " << mesh.nbTriangles() << std::endl;
}
